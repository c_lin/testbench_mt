/* 
   C. Lin, chiehlin@uchicago.edu
*/

module auto_live
(
// input 
  clk               , // system clock
  enable            ,
  q                 ,
  spill_cnt                                            
);

input wire         clk;
input wire         enable;

// output
output reg         q;
output reg [15 :0] spill_cnt;

//
reg [27:0] tiktok = 0;

////////////////////////////////////////////
always @(posedge clk) begin

   tiktok <= tiktok + 1; 
   q <= (tiktok[27]==1'b1 ) ? 1'b1 : 1'b0;
   spill_cnt <= (tiktok==28'hEFF_FFFF && spill_cnt<16'hFFFF ) 
                 ? (spill_cnt+1) : spill_cnt; 

   if( enable==1'b0 ) begin
      spill_cnt <= 0;
      tiktok <= 0;
   end
    
end

endmodule