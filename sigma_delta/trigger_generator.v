/* 
   C. Lin, chiehlin@uchicago.edu
*/

module trigger_generator
(
// input 
  clk               , // system clock
  reset             ,
  user_type         ,
  user_ntrig        ,
  user_interval     ,               
  
  // output
  out_ntrig,
  q                                   
);

input wire         clk;
input wire         reset;
input wire  [2 :0] user_type;
input wire  [15:0] user_ntrig;
input wire  [15:0] user_interval;

// output
output reg  [15 :0] out_ntrig;
output reg         q;

//
reg [3 :0] pattern = 0;
reg [16 :0] cnt = 0;

////////////////////////////////////////////
always @(posedge clk) begin

   if( reset==1'b1 ) begin
      cnt <= 0;
      out_ntrig <= 0;
   end

   // pattern definitions //
   case( user_type ) 
      // L1A
      3'b000: pattern <= 4'b0001;
      // align
      3'b001: pattern <= 4'b0101;
      // delta
      3'b010: pattern <= 4'b1001;
      // pL1A
      3'b011: pattern <= 4'b0111;
      // not defined
      default: pattern <= 4'b1111;
   endcase

   // trigger output
   if( out_ntrig < user_ntrig ) begin
      if( cnt < 4 ) begin
         q <= pattern[cnt];
      end
      else begin
         q <= 0;
      end
      
      out_ntrig <= (cnt==3) ? out_ntrig + 1 : out_ntrig;
      cnt <= (cnt < user_interval+4) ? (cnt + 1) : 0;
   end
    
end

endmodule